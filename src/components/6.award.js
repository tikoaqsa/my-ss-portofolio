import React, { Component } from 'react';
import { ImgCertificate } from '../assets';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCertificate } from '@fortawesome/free-solid-svg-icons';

export default class Award extends Component {
    render() {
        const myCertificate = [
            { name: 'VMWare Certificate', icon: faCertificate },
            { name: 'Secure Software Development Life Cycle Certificate', icon: faCertificate },
        ]
        return (
            <div className="app">
                <ImgCertificate />
                <div className='title'>Awards and Certificates</div>
                <div className='subtitle'>I got some certificates 📄</div>
                <div className='subtitle-small'>But never been awarded an award</div>

                {myCertificate.map((skill, index) => (
                    <div key={index} style={{ margin: '10px 0' }}> <FontAwesomeIcon className='icon' style={{ fontSize: '20px', color: '#532e1c' }} icon={skill.icon} />&nbsp;&nbsp;{skill.name}</div>
                ))}
            </div>
        )
    }
}
