import React, { Component } from 'react';
import { ImgPortofolio } from '../assets';

export default class Home extends Component {
    render() {
        return (
            <div className="app">
                <ImgPortofolio />
                <div className='title'>Hello World</div>
                <div className='subtitle'>My name is Tiko Aqsa Alif Nugroho 🙆🏽‍♂️</div>
                <div className='subtitle-small'>Software Engineer and Civil Servant in Indonesia National Single Window</div>
                <div className='subtitle-smaller'>Ministry of Finance of Republic of Indonesia</div>
            </div>
        )
    }
}
