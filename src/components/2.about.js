import React, { Component } from 'react';
import { ImgAboutMe } from '../assets';

export default class About extends Component {
    render() {
        return (
            <div className="app">
                <ImgAboutMe />
                <div className='title'>About me</div>
                <div className='subtitle'>I was born in Jakarta, Indonesia 🇮🇩</div>
                <div className='subtitle-small'>As the one of the last baby born in the 20th century</div>
            </div>
        )
    }
}
