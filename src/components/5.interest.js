import React, { Component } from 'react';
import { ImgSearching } from '../assets';

export default class Interest extends Component {
    render() {
        return (
            <div className="app">
                <ImgSearching />
                <div className='title'>Interest</div>
                <div className='subtitle'>One thing that has always been interesting to me is <b>knowledge 💡</b></div>
                <div className='subtitle-small'>Because, the more you know, the more you are understanding</div>
            </div>
        )
    }
}
