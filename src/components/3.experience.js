import React, { Component } from 'react';
import Timelines from './components/timelines';

export default class Experience extends Component {
    render() {
        return (
            <div className="app">
                <Timelines />
                <div className='title'>Experience</div>
                <div className='subtitle'>My experince on my productive time ⏱</div>
                <div className='subtitle-small'>My experience is mostly in data and programming</div>
            </div>
        )
    }
}
